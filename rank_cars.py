#!/usr/bin/env python

from argparse import ArgumentParser
from sys import stdin
import pandas as pd
import networkx as nx
from toposort import toposort
from pprint import pprint

arg_parser = ArgumentParser()

arg_parser.add_argument('--min-concordance', type=float, default=0.9)
arg_parser.add_argument('--max-discordance', type=float, default=0.1)

arg_parser.add_argument('--price-weight', type=float, default=7)
arg_parser.add_argument('--top-speed-weight', type=float, default=3)
arg_parser.add_argument('--year-built-weight', type=float, default=5)
arg_parser.add_argument('--mileage-weight', type=float, default=6)
arg_parser.add_argument('--fuel-consumption-weight', type=float, default=4)

arg_parser.add_argument('--verbose', default=False, action='store_true')

args = arg_parser.parse_args()


cars = pd.read_csv(stdin)
cars.set_index('id', inplace=True)
if args.verbose:
    pprint(cars)
    print()

param_orders = {
    'price': lambda a, b: a < b,
    'top_speed': lambda a, b: a > b,
    'year_built': lambda a, b: a > b,
    'mileage': lambda a, b: a < b,
    'fuel_consumption': lambda a, b: a < b,
}
param_groups = pd.DataFrame(columns=['id_a', 'id_b', *cars.columns], dtype=int)
for a, car_a in cars.iterrows():
    for b, car_b in cars.iterrows():
        row = [a, b]
        for param in cars.columns:
            if param_orders[param](car_a[param], car_b[param]):
                row.append('+')
            elif param_orders[param](car_b[param], car_a[param]):
                row.append('-')
            else:
                row.append('=')
        param_groups.loc[len(param_groups)] = row
param_groups[['id_a', 'id_b']] = param_groups[['id_a', 'id_b']].astype(int)
param_groups.set_index(['id_a', 'id_b'], inplace=True)
if args.verbose:
    pprint(param_groups)
    print()

param_weights = pd.DataFrame([
    ('price', args.price_weight),
    ('top_speed', args.top_speed_weight),
    ('year_built', args.year_built_weight),
    ('mileage', args.mileage_weight),
    ('fuel_consumption', args.fuel_consumption_weight),
], columns=['param', 'weight'])
param_weights.set_index('param', inplace=True)
if args.verbose:
    pprint(param_weights)
    print()

param_spans = cars.max() - cars.min()
total_weight = param_weights['weight'].sum()
cordances = pd.DataFrame(columns=['id_a', 'id_b', 'concordance', 'discordance'])
for id_a, id_b in param_groups.index:
    concordance = 0
    discordance = 0
    for param, weight in param_weights['weight'].items():
        if param_groups.get_value((id_a, id_b), param) in ('+', '='):
            concordance += weight
        else:
            discordance = max(discordance, abs(cars.get_value(id_a, param) - cars.get_value(id_b, param)) / param_spans[param])
    concordance = concordance / total_weight
    cordances.loc[len(cordances)] = [id_a, id_b, concordance, discordance]
cordances[['id_a', 'id_b']] = cordances[['id_a', 'id_b']].astype(int)
cordances.set_index(['id_a', 'id_b'], inplace=True)
if args.verbose:
    pprint(cordances)
    print()

if args.verbose:
    pprint(pd.DataFrame([
        args.min_concordance,    
        args.max_discordance,    
    ], columns=[''], index=['min_concordance', 'mac_discordance']))
    print()

def dominates(id_a, id_b):
    concordance, discordance = cordances.loc[(id_a, id_b)]
    return concordance >= args.min_concordance and discordance <= args.max_discordance
dominance = {id_a: {id_b for id_b in cars.index
                         if dominates(id_a, id_b)} for id_a in cars.index}
if args.verbose:
    pprint(pd.DataFrame(
        ((n1, n2, n2 in dominance[n1]) for n1 in cars.index
                                       for n2 in cars.index),
        columns=['id_a', 'id_b', 'dominates'],
    ).set_index(['id_a', 'id_b']))
    print()

print('ranking')
ranking = reversed(list(toposort(dominance)))
ranking = {i: sorted(group) for i, group in enumerate(ranking)}
pprint(ranking)

