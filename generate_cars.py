#!/usr/bin/env python

from argparse import ArgumentParser
from random import Random
import pandas as pd
from sys import stdout

arg_parser = ArgumentParser()

arg_parser.add_argument('--seed', type=str, default='')
arg_parser.add_argument('--price-mu', type=float, default=15000)
arg_parser.add_argument('--price-sigma', type=float, default=3000)
arg_parser.add_argument('--top-speed-mu', type=float, default=250)
arg_parser.add_argument('--top-speed-sigma', type=float, default=30)
arg_parser.add_argument('--year-built-mu', type=float, default=1990)
arg_parser.add_argument('--year-built-sigma', type=float, default=20)
arg_parser.add_argument('--mileage-mu', type=float, default=100000)
arg_parser.add_argument('--mileage-sigma', type=float, default=20000)
arg_parser.add_argument('--fuel-consumption-mu', type=float, default=15)
arg_parser.add_argument('--fuel-consumption-sigma', type=float, default=5)
arg_parser.add_argument('--count', type=int, default=50)

args = arg_parser.parse_args()

random = Random(args.seed)

def generate_price():
    return int(random.normalvariate(args.price_mu, args.price_sigma)) // 100 * 100

def generate_top_speed():
    return int(random.normalvariate(args.top_speed_mu, args.top_speed_sigma)) // 10 * 10

def generate_year_built():
    return int(random.normalvariate(args.year_built_mu, args.year_built_sigma))

def generate_mileage():
    return int(random.normalvariate(args.mileage_mu, args.mileage_sigma)) // 100 * 100

def generate_fuel_consumption():
    return int(random.normalvariate(args.fuel_consumption_mu, args.fuel_consumption_sigma) * 10) / 10

cars = pd.DataFrame(columns=[
    'price',
    'top_speed',
    'year_built',
    'mileage',
    'fuel_consumption',
])
for i in range(args.count):
    cars.loc[i] = (
        generate_price(),
        generate_top_speed(),
        generate_year_built(),
        generate_mileage(),
        generate_fuel_consumption(),
    )
cars.index.name = 'id'

cars.to_csv(stdout)

